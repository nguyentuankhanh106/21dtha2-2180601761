﻿using Lab4.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using Lab4.Models;
using System.Reflection;

namespace Lab4
{
    public partial class Form3 : Form
    {
        private StudentContextDB context;
        public Form3(StudentContextDB context)
        {
            InitializeComponent();
            this.context = context;
        }
        private void FillFacultyCombobox(List<Faculty> listFalcultys)
        {
            this.cboKhoa.DataSource = listFalcultys;
            this.cboKhoa.DisplayMember = "FacultyName";
            this.cboKhoa.ValueMember = "FacultyID";
        }

        private void BindGrid(List<Student> listStudent)
        {
            dataGridView1.Rows.Clear();
            foreach (var item in listStudent)
            {
                int index = dataGridView1.Rows.Add();
                dataGridView1.Rows[index].Cells[0].Value = item.FacultyID;
                dataGridView1.Rows[index].Cells[1].Value = item.FullName;
                dataGridView1.Rows[index].Cells[2].Value = item.Faculty.FacultyName;
                dataGridView1.Rows[index].Cells[3].Value = item.AverageScore;
            }
        }
        private void Form3_Load(object sender, EventArgs e)
        {
            try
            {
                List<Faculty> listFacuty = context.Faculties.ToList();
                List<Student> listStudent = context.Students.Include("Faculty").ToList();
                FillFacultyCombobox(listFacuty);
                //BindGrid(listStudents);*/
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            try
            {
                List<Faculty> listFaculty = context.Faculties.ToList();
                var query = from sv in context.Students.Include("Faculty")
                            select sv;

                if (!string.IsNullOrEmpty(txtMSSV.Text.Trim()))
                {
                    query = query.Where(sv => sv.StudentID.Contains(txtMSSV.Text.Trim()));
                }

                if (!string.IsNullOrEmpty(txtTenSV.Text.Trim()))
                {
                    query = query.Where(sv => sv.FullName.Contains(txtTenSV.Text.Trim()));
                }

                if (cboKhoa.SelectedIndex >= 0)
                {
                    int maKhoa = ((Faculty)cboKhoa.SelectedItem).FacultyID;
                    query = query.Where(sv => sv.FacultyID == maKhoa);
                }
                List<Student> listStudents = query.ToList();
                BindGrid(listStudents);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                // Xóa tất cả các điều kiện tìm kiếm
                txtMSSV.Clear();
                txtTenSV.Clear();
                cboKhoa.SelectedIndex = -1;
                // Hiển thị lại tất cả sinh viên lên DataGridView
                List<Student> listStudents = context.Students.Include("Faculty").ToList();
                BindGrid(listStudents);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void btnTroVe_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}