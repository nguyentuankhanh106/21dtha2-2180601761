﻿using Lab4.Models;
using Lab4.Report.StudenReport;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab4
{
    public partial class FrmReport : Form
    {
        public FrmReport()
        {
            InitializeComponent();
        }

        private void FrmReport_Load(object sender, EventArgs e)
        {

            StudentContextDB db = new StudentContextDB();
            var listStudentReportDto = db.Students.Select(c => new StudentReportDto
            {
                StudentID = c.StudentID,
                FullName = c.FullName,
                AverageScore = c.AverageScore,
                FacultyName = c.Faculty.FacultyName,

            }).ToList();
            this.reportViewer.LocalReport.ReportPath = "rptStudentReport.rdlc";
            var reportDataSource = new ReportDataSource("StudentReportDataSet", listStudentReportDto);
            this.reportViewer.LocalReport.DataSources.Clear();
            this.reportViewer.LocalReport.DataSources.Add(reportDataSource);
            this.reportViewer.RefreshReport();
        }

    }
}
